<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/add_styles.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <title>Document</title>
</head>
<body>
    <?php
    include "database.php";
    include "sqlwork.php";
    include "product.php";
    ?>
    <div class = "mainbox">
        <header>
            <div id="spacehead">Product Add</div>
            <div><button type="submit" form="product_form">Save</button></div>
            <div><button onclick="location.href='index.php';">Cancel</button></div>
        </header>
        <hr>
        <div class = "productaddbox">
            <form action="create_product.php" id="product_form" method="post" onsubmit="checkValues()">
                <label for="sku">SKU</label>
                <input type="text" name="sku" id="sku" class="value-required"><br>
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="value-required"> <br>
                <label for="price">Price</label>
                <input type="number" step="0.01" name="price" id="price" class="value-required"> <br>
                <label for="productType">Type switcher</label>
                <select id="productType" name="type" onchange="showHideProperties()">  
                    <option value="empty">...</option>  
                    <option value="Book">Book</option>  
                    <option value="DVD">DVD</option>  
                    <option value="Furniture">Furniture</option>   
                </select> <br>
                <div id="product-div">
                    <div id="empty-div" class="active-div">
                        <span class="tip">Please choose type of product</span> <br>
                    </div>
                    <div id="book-div">
                        <label for="weight">Weight, kg</label>
                        <input type="number" step="0.1" name="weight" id="weight"> <br>
                        <span class="tip">Please enter weight of book in kg</span> <br>
                    </div>
                    <div id="DVD-div">
                        <label for="size">Size, MB</label>
                        <input type="number" name="size" id="size"> <br>
                        <span class="tip">Please enter size of DVD in Mb</span> <br>
                    </div>
                    <div id="furniture-div">
                        <label for="height">Height, cm</label>
                        <input type="number" name="height" id="height"> <br>
                        <label for="width">Width, cm</label>
                        <input type="number" name="width" id="width"> <br>
                        <label for="length">Length, cm</label>
                        <input type="number" name="length" id="length"> <br>
                        <span class="tip">Please enter dimensions of furniture in cm</span> <br>
                    </div>
                </div>
            </form>
        </div>
        <hr>
        <footer>
            Scandiweb Test assignment
        </footer>
    </div>
    <script src="js/add_script.js"></script>
</body>
</html>
