<?php
class Database
{
    private $servername = "localhost";
    private $dbname = "id19303016_testdb2";
    private $username = "id19303016_root";
    private $password = "Root1234!@#$";
    public $conn;
    public function getConnection()
    {
        $this->conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        if ($this->conn -> connect_error) {
            die("Connection failed" . $this->conn->connect_error);
        }
        return $this->conn;
    }
}
