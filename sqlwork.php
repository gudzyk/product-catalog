<?php
// class with all database manipulations
class SQLWork
{
    // add new product to database
    public static function setToSQL($params)
    {
        $db = new Database();
        $conn = $db->getConnection();
        $sql = "INSERT INTO products (sku, name, price, type, property1, property2, property3)
         VALUES ($params)";
        if ($conn->query($sql) !== TRUE) {
            $conn->error;
        }
        $conn->close();
    }
    // get all products from database
    public static function getFromSQL()
    {
        $db = new Database();
        $conn = $db->getConnection();
        $sql = "SELECT * FROM products ORDER BY sku ASC";
        $result = $conn->query($sql);
        $conn->close();
        return $result;
    }

    public static function buildIndexPage()
    {
        $products = SQLWork::getFromSQL();
        while ($row = $products->fetch_assoc()) {
            echo '<div class="productcell"> ';
            echo '<input type="checkbox" class="delete-checkbox" del-sku="'. $row["sku"] .'" > ';
            echo $row["sku"] .'<br>' . $row["name"] . '<br>' . number_format($row["price"], 2) . ' $<br>';
            $types = ["book"=>"Book", "DVD"=>"DVD", "furniture"=>"Furniture"];
            $valClass = $types[$row["type"]];
            $obj = new $valClass();
            $descr = $obj->getDescr($row["property1"], $row["property2"], $row["property3"]);
            echo $descr;
            echo '</div>';
        }
    }
    // delete product from database by sku
    public static function deleteFromSQL($sku)
    {
        $db = new Database();
        $conn = $db->getConnection();
        $sql = "DELETE FROM `products` WHERE `products`.`sku` = '".$sku."'";
        $conn->query($sql);
        $conn->close();
    }
}
