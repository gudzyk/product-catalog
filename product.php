<?php
abstract class Product
{
    protected $sku;
    protected $name;
    protected $price;
    protected $type;
    // set properties of object and add it to database
    public function setAttrs($sku, $name, $price, $attr0, $attr1, $attr2) {}
    // format description text for index page
    public function getDescr($attr0, $attr1, $attr2) {}
    // set properties of object (received from ADD page) and add it to database
    public function addProduct($array) {}
}
class Book extends Product
{
    private $weight;
    // set properties of object and add it to database
    public function setAttrs($sku, $name, $price, $weight, $attr1=0, $attr2=0)
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->weight = $weight;
        $this->type = "book";
        $params = "'$this->sku', '$this->name', '$this->price', '$this->type', '$this->weight', '0', '0'";
        SQLWork::setToSQL($params);
    }
    // format description text for index page
    public function getDescr($weight, $attr1, $attr2)
    {
        $this->weight = $weight;
        return "Weight: ".$this->weight." kg";
    }
    // set properties of object (received from ADD page) and add it to database
    public function addProduct($array)
    {
        $this->setAttrs($array['sku'], $array['name'], $array['price'], $array['weight'], 0, 0);
    }
}
class DVD extends Product
{
    private $size;
    // set properties of object and add it to database
    public function setAttrs($sku, $name, $price, $size, $attr1=0, $attr2=0)
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->size = $size;
        $this->type = "DVD";
        $params = "'$this->sku', '$this->name', '$this->price', '$this->type', '$this->size', '0', '0'";
        SQLWork::setToSQL($params);
    }
    // format description text for index page
    public function getDescr($size, $attr1, $attr2)
    {
        $this->size = $size;
        return "Size: ".$this->size." Mb";
    }
    // set properties of object (received from ADD page) and add it to database
    public function addProduct($array)
    {
        $this->setAttrs($array['sku'], $array['name'], $array['price'], $array['size'], 0, 0);
    }
}
class Furniture extends Product
{
    private $height;
    private $width;
    private $length;
    // set properties of object and add it to database
    public function setAttrs($sku, $name, $price, $height, $width, $length)
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
        $this->type = "furniture";
        $params = "'$this->sku', '$this->name', '$this->price', '$this->type', '$this->height', '$this->width', '$this->length'";
        SQLWork::setToSQL($params);
    }
    // format description text for index page
    public function getDescr($height, $width, $length)
    {
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
        return "Dimension: " . $this->height . "x" . $this->width . "x" . $this->length;
    }
    // set properties of object (received from ADD page) and add it to database
    public function addProduct($array)
    {
        $this->setAttrs($array['sku'], $array['name'], $array['price'], $array['height'], $array['width'], $array['length']);
    }
}
