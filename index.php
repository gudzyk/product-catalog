<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <title>Document</title>
</head>
<body>
    <?php
    include "database.php";
    include "sqlwork.php";
    include "product.php";
    ?>
    <div class = "mainbox">
        <header>
            <div id="spacehead">Product List</div>
            <div><button id="add-product-btn" onclick="location.href='addproduct.php'">ADD</button></div>
            <div><button id="delete-product-btn">MASS DELETE</button></div>
        </header>
        <hr>
        <div class = "productbox">
            <?php
            SQLWork::buildIndexPage();
            ?>
        </div>
        <hr>
        <footer>
            Scandiweb Test assignment
        </footer>
    </div>
    <script src="js/script.js"></script>
</body>
</html>
