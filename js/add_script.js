// show form with properties of choosen product type and hide anothers
function showHideProperties() {
    let val = document.querySelector("#productType").value;
    let typeArr = ["empty", "Book", "DVD", "Furniture"];
    let divArr = ["#empty-div", "#book-div", "#DVD-div", "#furniture-div"];
    let index = typeArr.indexOf(val);
    for (let i = 0; i < 4; i++) {
        document.querySelector(divArr[i]).style.display = 'none';
        document.querySelector(divArr[i]).className = '';
    }
    document.querySelector(divArr[index]).style.display = 'block';
    document.querySelector(divArr[index]).className = 'active-div';
    let inputArr = document.querySelector("#product-div").querySelectorAll("input");
    for (let i = 0; i < inputArr.length; i++) {
        inputArr[i].className = "";
    }
    inputArr = document.querySelector(divArr[index]).querySelectorAll("input");
    for (let i = 0; i < inputArr.length; i++) {
        inputArr[i].className = "value-required";
    }
}

// validate values in form fields before submit
function checkValues() {
    let errAlert = "";
    // get list of existing sku from local storage
    // and check if sku of creating product already exists
    let skuArr = JSON.parse(localStorage.getItem("skus"));
    let sku = document.querySelector("#sku").value;
    if (skuArr.indexOf(sku) > -1) {
        errAlert = "This sku is already exists. ";
    }
    // check if all fields is not empty
    let inputArr = document.querySelectorAll(".value-required");
    let errorArr = [];
    for (let i = 0; i < inputArr.length; i++) {
        if (inputArr[i].value == "") {
            errorArr.push(inputArr[i].id);
        }
    }
    // check if type product is not empty
    if (document.querySelector("#productType").value == "empty") {
        errorArr.push("type switcher");
    }
    // format text in error message
    if (errorArr.length > 0) {
        errAlert = errAlert + "Please, submit required data: ";
        for (let i = 0; i < errorArr.length - 1; i++) {
            errAlert = errAlert + errorArr[i] + ", ";
        }
        errAlert = errAlert + errorArr[errorArr.length - 1];
    }
    // if there are errors, output error message and stop submitting
    if (errAlert != "") {
        let activeTip = document.querySelector(".active-div").querySelector(".tip");
        activeTip.textContent = errAlert;
        activeTip.style.color = "red";
        event.preventDefault();
    }
}
