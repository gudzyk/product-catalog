document.querySelector("#delete-product-btn").addEventListener("click", deleteProducts);
document.querySelector("#add-product-btn").addEventListener("click", getSkus);

// send sku of all checked products to delete them from database
function deleteProducts() {
    let list = document.querySelectorAll(".delete-checkbox");
    let arr = [];
    for (const item of list) {
        if (item.checked) {
            arr.push(item.getAttribute("del-sku"));
        }
    }
    $.ajax({
        type: "POST",
        url: 'delete_products.php',
        data: { 'variable': arr }
    });
    window.location = document.URL;
}

// put SKU list in local storage before redirect to ADD page
function getSkus() {
    let list = document.querySelectorAll(".delete-checkbox");
    let arr = [];
    for (const item of list) {
        arr.push(item.getAttribute("del-sku"));
    }
    localStorage.setItem("skus", JSON.stringify(arr));
}
